private ["_group", "_veh", "_type", "_paramCount", "_vpos", "_distOffset", "_totalSeats"];
_paramCount = count _this;
if (_paramCount > 0) then {
	_group = _this select 0;
	_veh   = _this select 1;
	_distOffset = 10;

	if (_paramCount == 3) then {
		_type = _this select 2;
	} else { _type = ""; };

	// Default.
	_group setFormation "FILE";
	_vpos = getPos _veh;
	_group move _vpos;
	_group addVehicle _veh;
	_totalSeats = [typeof _veh, true] call BIS_fnc_crewCount;
	{
		_x assignAsCargo _veh;
		[_x] orderGetIn true;
		[_x] allowGetIn true;
		_x action ["WeaponOnBack", _x];
	} forEach units _group;

	waitUntil { (count (crew _veh)) == _totalSeats };
	if ((count (units _group)) > _totalSeats) then {
		_group setFormation "STAG COLUMN";
	};
};