private ["_person", "_pos", "_off", "_veh"];
_person = _this select 0;
_pos = _this select 1;
_off = _this select 2;
_veh = _this select 3;
waitUntil {(_person distance _pos) < _off};

_person assignAsCargo _veh;
[_person] orderGetIn true;
[_person] allowGetIn true;
