private ["_vehicles", "_location", "_leadVeh", "_dispersion", "_speedLimit", "_grp", "_prevVeh"];
_vehicles = _this select 0;
_location = _this select 1;

_dispersion = 10;
_speedLimit = 30;
_sleepInterval = 1;
_convoyTime = 0;
_leadVehicleSpeed = 0;

_vehicleCount = count _vehicles;
if (_vehicleCount > 0) then {
	_leadVeh  = _vehicles select 0;
	_leadSide = side _leadVeh;
	_grp 	  = createGroup _leadSide;

	_prevVeh = '';
	_leadVehicleSpeed = _speedLimit;

	while { _leadVeh distance2D _location > 0 } do {

		{
			if (_leadVeh isEqualTo _x) then {
				_wp = (group _leadVeh) addWaypoint [_location, 0];
				_wp setWaypointType "MOVE";
				_wp setWaypointBehaviour "SAFE";
			} else {
				// 	// Do something for other vehicles.
			};

			// For every cars except last one.
			if (_forEachIndex < (_vehicleCount - 1)) then {
				// Check if the vehicle behind is not lagging up.
				_nextIndex = _forEachIndex + 1;
				if (_x distance2D (_vehicles select _nextIndex) > _dispersion) then {
					_leadVehicleSpeed = _leadVehicleSpeed - 2; // deduct 2km/h
					if (_leadVehicleSpeed <= 0) then { _leadVehicleSpeed = 0; };
				} else {
					_leadVehicleSpeed = _speedLimit;
				};

				_x limitSpeed _leadVehicleSpeed;
			};

		} foreach _vehicles;

		_convoyTime = _convoyTime + _sleepInterval;
		hint format ["Time: %1 sec. Convoy speed: %2 km/h", _convoyTime, speed _leadVeh];
		sleep _sleepInterval;
	};



	// // Set some properties on the vehicles.
	// { 
	// 	if (_first) then {
	// 		_first = false;
	// 		_leadVeh commandMove _location;
	// 		_prevVeh = _leadVeh;
	// 	} else {
	// 		[_x, _prevVeh] execVM "a3a\convoy\followLead.sqf";
	// 		_prevVeh = _x;
	// 	};


	// 	// _x forceFollowRoad true;  // Set all vehicles to follow the road, pronto.
	// 	// _x limitSpeed _speedLimit;	// Set speed limit.
	// 	// [_x] join _grp; // Join group

	// } foreach _vehicles;

	// // Set group behavior to safe.
	// _grp setBehaviour "SAFE";

	// Wait until lead vehicle has arrived.

};