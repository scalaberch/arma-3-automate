private ["_me", "_front", "_disp"];

_me    = _this select 0;
_front = _this select 1;

_disp   = 30;
waitUntil { (_me distance2D _front) >= _disp };
_prevWP = '';

hint 'moving!';
while { true } do {
	_wp = (group _me) addWaypoint [position _front, 0];
	_wp setWaypointType "MOVE";
	_wp setWaypointBehaviour "SAFE";
	sleep 2;
	hint 'added waypoint!';
};



// while { true } do {

// };