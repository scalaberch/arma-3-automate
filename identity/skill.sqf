/***
* aimingAccuracy - Affects the probability of the AI hitting a target with its weapon(s)
* aimingShake - Affects the AI weapon sway, in this case meaning that 0.10 for example sets the weapon at 90% sway
* aimingSpeed - Affects the AI's ability to place its crosshair/iron sight exactly where it needs to hit the target, higher means quicker aim
* reloadSpeed - How quickly an AI reloads his weapon, higher means faster reload time
* spotDistance - How many percent out of maximum view distance can the AI spot the target from?
* spotTime - How quickly does the AI spot the player with a direct line of sight? 1.0 being immediately.
* courage - How courageous is the AI? This is affected by a number of criteria, such as friends dead. 1.0 means that it will never try to run away.
* commanding - How effective is the AI at, if it can, communicate that it has spotted a hostile target and its position to the other AI? 1.0 = 100% effective.
*/

// [["aimingAccuracy",0.10],["aimingShake",0.70],["aimingSpeed",0.75],["spotDistance",0.70],["spotTime",0.50],["courage",1.00],["reloadSpeed",1.00],["commanding",0.50]];

private ["_unit", "_skillsetValue", "_rank", "_seed", "_multiplier", "_type", "_listcount"];

_listcount 	   = count _this;
_skillsetValue = 0;

if (_listcount >= 1) then {
	_rank = _this select 0;
	if (_listcount >= 2) then { _type = _this select 1; } else { _type = ""; }

	_seed       = 0;
	_multiplier = 0;

	switch(_type) do {
		case "militia": { _seed = 0.125; }
		case "specop": { _seed = 0.70; }
		default: { _seed = 0.5; };
	}
};


switch (_rank) do {
	case 'COL': { = 0.8 };
	case 'LTC': { = 1 };
	case 'MAJ': { = 1 };
	case 'CPT': { = 1 };
	case '1LT': { = 1 };
	case '2LT': { = 0.4 };
	case 'PVT': { = 0.38 };
	case 'PFC': { = 0.39 };
	case 'CPL': { = 0.4 };
	case 'SGT': { = 1 };
	case 'SSG': { = 1 };
	case 'TSG': { = 1 };
	case 'MSG': { = 1 };
	case 'SGM': { = 1 };
}




_skillsetValue