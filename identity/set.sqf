private ["_unit", "_name", "_rank", "_race"];
_paramCount = count _this;
if (_paramCount >= 2) then {
	_unit = _this select 0;
	_name = _this select 1;

	if (_paramCount >= 3) then { _rank = _this select 2; } else { _rank = ""; };
	if (_paramCount >= 4) then { _race = _this select 3; } else { _race = ""; };

	// Set the name.
	if !(_rank isEqualTo "") then { _name = _rank + " " + _name; };
	_unit setName _name;

	// Set the rank.
	_actualRank = _rank;
	switch(_actualRank) do {
		case "PVT"; case "PFC": {
			_unit setRank "PRIVATE";
		};
		case "CPL"; case "SPC"; case "LCPL": {
			_unit setRank "PRIVATE";
		};
		case "SGT"; case "SSG"; case "SFC"; 
		case "MSG"; case "TSG"; case "SGM": {
			_unit setRank "SERGEANT";
		};
		case "2LT"; case "1LT"; case "LT": {
			_unit setRank "LIEUTENANT";
		};
		case "CPT": { _unit setRank "CAPTAIN"; };
		case "MAJ": { _unit setRank "MAJOR"; };
		case "LTC"; case "COL";
		case "BG"; case "MG"; case "LTG";
		case "GEN": { _unit setRank "COLONEL"; };
		default {}; 
	}

};

// Usage:
// null = [s1, "Name", "PVT", "PH", 0] call "a3a/identity/set.sqf";