private ["_unit", "_race", "_face", "_voice", "_facelist", "_voicelist"];
_unit = _this select 0;
_race = _this select 1;

_facelist  = [];
_voicelist = [];

_race = toUpper(_race);
switch (_race) do {
	case "PH": {
		_facelist  = ["AsianHead_A3_01", "AsianHead_A3_02", "AsianHead_A3_03"];
		_voicelist = [];
	};
	case "CN": {

	};
	case "RU": {

	};
	case "US": {

	};
	case "UK": {

	};
};

_face  = "";
_voice = "";

_unit setFace _face;
_unit setSpeaker _voice;